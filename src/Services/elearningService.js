import axios from "axios";
import { BASE_URL, createConfig } from "./configURL";

export const elearningService = {
  getDanhSachKhoaHoc: () => {
    return axios({
      url: `${BASE_URL}/api/QuanLyKhoaHoc/LayDanhSachKhoaHoc?MaNhom=GP01`,
      method: "GET",
      headers: createConfig(),
    });
  },
  getDanhMucKhoaHoc: () => {
    return axios({
      url: `${BASE_URL}/api/QuanLyKhoaHoc/LayDanhMucKhoaHoc?MaNhom=GP01`,
      method: "GET",
      headers: createConfig(),
    });
  },
  getKhoaHocTheoDanhMuc: (paramDanhMuc) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyKhoaHoc/LayKhoaHocTheoDanhMuc?maDanhMuc=${paramDanhMuc}&MaNhom=GP01`,
      method: "GET",
      headers: createConfig(),
    });
  },
  getChiTietKhoaHoc: (paramChiTiet) => {
    return axios({
      url: `${BASE_URL}/api/QuanLyKhoaHoc/LayThongTinKhoaHoc?maKhoaHoc=${paramChiTiet}`,
      method: "GET",
      headers: createConfig(),
    });
  },
};
