import React, { useEffect, useState } from "react";
import { elearningService } from "../../Services/elearningService";
import { useParams } from "react-router-dom";
import CarouselItemSearch from "../../Components/CarouselItemSearch/CarouselItemSearch";

export default function SearchPage() {
  const [dataElearning, setDataElearning] = useState([]);
  const [renderItems, setRenderItems] = useState([]);

  const paramSearch = useParams();

  useEffect(() => {
    elearningService
      .getDanhSachKhoaHoc()
      .then((res) => {
        setDataElearning(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);
  const renderSearchList = () => {
    let newArr = dataElearning.filter((item) =>
      item.tenKhoaHoc
        .toLowerCase()
        .includes(paramSearch.idSearch.trim().toLowerCase())
    );

    return <CarouselItemSearch dataElearning={newArr} />;
  };

  return (
    <div id="DanhSachKhoaHoc" className="container mx-auto">
      <h1 className="text-2xl text-center mt-5 mb-5 font-bold">
        TÌM KIẾM KHOÁ HỌC: {paramSearch.idSearch}
      </h1>
      {renderSearchList()}
    </div>
  );
}
