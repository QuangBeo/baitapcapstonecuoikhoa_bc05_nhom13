import React, { useEffect, useState } from "react";
import { elearningService } from "../../Services/elearningService";
import { useParams } from "react-router-dom";
import CarouselItemDanhMuc from "../../Components/CarouselItems/CarouselItemDanhMuc";

export default function ListCategory() {
  const [dataDanhMuc, setDataDanhMuc] = useState([]);
  let param = useParams();
  console.log("param: ", param);
  useEffect(() => {
    elearningService
      .getKhoaHocTheoDanhMuc(param.idDanhMuc)
      .then((res) => {
        console.log(res);
        setDataDanhMuc(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [param.idDanhMuc]);

  return (
    <div>
      <div
        style={{ background: "linear-gradient(90deg,#571642,#8D2C42" }}
        className=" py-8 pl-52 text-gray-200 text-4xl"
      >
        {param.idDanhMuc}
      </div>
      <div className="container mx-auto">
        <h1 className="text-2xl text-center mt-5 mb-5 font-bold">
          CÁC KHOÁ HỌC PHỔ BIẾN
        </h1>
        <CarouselItemDanhMuc dataDanhMuc={dataDanhMuc} />
      </div>
    </div>
  );
}
