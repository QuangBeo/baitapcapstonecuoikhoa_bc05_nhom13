import React from "react";
import { Button, Form, Input, message } from "antd";
import { userSevices } from "../../Services/userSevices/userSevices";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import { SET_USER_INFOR } from "../../Redux/constants/userConstants";
import { userLocalService } from "../../Services/localStoreServices/localStoreServices";
import Lottie from "lottie-react";
import animateBG from "../../Assets/LoginPageTab/88331-happy-new-year-pink-loader-preloader-transparent.json";

export default function LoginPage() {
  const navigate = useNavigate();
  const dispatch = useDispatch();
  const onFinish = (values) => {
    console.log("Success:", values);
    userSevices
      .postDangKy(values)
      .then((res) => {
        console.log(res);
        userLocalService.set(res.data);
        message.success("Đăng Ký thành công");
        setTimeout(() => {
          dispatch({
            type: SET_USER_INFOR,
            payload: res.data,
          });
          userLocalService.set(res.data);
          navigate("/");
        });
      })
      .catch((err) => {
        console.log(err);
        message.error("Đăng Ký thất bại");
      });
  };
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div className="w-screen h-screen flex justify-center items-center">
      <div className="container p-5 flex justify-center items-center">
        <div className="h-full w-1/2">
          <Lottie animationData={animateBG} loop={true} />
        </div>
        <div className="h-full w-1/2">
          <Form
            name="basic"
            labelCol={{
              span: 8,
            }}
            wrapperCol={{
              span: 24,
            }}
            initialValues={{
              remember: true,
            }}
            onFinish={onFinish}
            onFinishFailed={onFinishFailed}
            autoComplete="off"
            layout="vertical"
          >
            <Form.Item
              label="Username"
              name="taiKhoan"
              rules={[
                {
                  required: true,
                  message: "Please input your username!",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              label="Password"
              name="matKhau"
              rules={[
                {
                  required: true,
                  message: "Please input your password!",
                },
              ]}
            >
              <Input.Password />
            </Form.Item>
            <Form.Item
              label="Họ và Tên"
              name="hoTen"
              rules={[
                {
                  required: true,
                  message: "Please input your Họ và Tên!",
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="SĐT"
              name="soDT"
              rules={[
                {
                  required: true,
                  message: "Please input your soDT!",
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="maNhom"
              name="maNhom"
              rules={[
                {
                  required: true,
                  message: "Please input your maNhom!",
                },
              ]}
            >
              <Input />
            </Form.Item>
            <Form.Item
              label="email"
              name="email"
              rules={[
                {
                  required: true,
                  message: "Please input your email!",
                },
              ]}
            >
              <Input />
            </Form.Item>

            <Form.Item
              wrapperCol={{
                offset: 8,
                span: 24,
              }}
            >
              <Button className="bg-red-500 text-white" htmlType="submit">
                Submit
              </Button>
            </Form.Item>
          </Form>
        </div>
      </div>
    </div>
  );
}
