import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { elearningService } from "../../Services/elearningService";
import CarouselDetail from "../../Components/Carousel/CarouselDetail";

export default function DetailPage() {
  const [dataDetail, setDataDetail] = useState([]);
  let param = useParams();
  useEffect(() => {
    elearningService
      .getChiTietKhoaHoc(param.id)
      .then((res) => {
        console.log(res);
        setDataDetail(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, [param.id]);

  return (
    <div>
      <CarouselDetail dataDetail={dataDetail} />
      <div className="container mx-auto py-10">
        <h1 className="mb-6 font-bold text-4xl">
          Khoá Học: {dataDetail.tenKhoaHoc}
        </h1>
        <div className="flex justify-left gap-10 mt-14">
          <img
            className="w-[650px] object-cover"
            src={dataDetail.hinhAnh}
            alt=""
          />
          <div>
            <h2 className="font-bold text-2xl">Mô tả: {dataDetail.moTa}</h2>
            <p className="py-8 text-xl font-medium">
              Lượt xem: {dataDetail.luotXem}
            </p>
            <p className="text-xl">
              CyberSoft khai thác nhu cầu tuyển dụng lập trình, kết nối việc làm
              tới doanh nghiệp và tích hợp các dự án với công nghệ mới nhất vào
              phương pháp đào tạo tích cực cho các học viên học xong có việc làm
              ngay. Chương trình giảng dạy được biên soạn may đo dành riêng cho
              các bạn Lập trình từ trái ngành hoặc đã có kiến thức theo cường độ
              cao, luôn được tinh chỉnh và tối ưu hóa theo thời gian bởi các
              thành viên sáng lập và giảng viên dày kinh nghiệm.
            </p>
          </div>
        </div>
      </div>
    </div>
  );
}
