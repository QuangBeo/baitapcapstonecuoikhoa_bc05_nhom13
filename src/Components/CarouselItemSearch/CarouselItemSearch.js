import React, { useState } from "react";
import { Card } from "antd";
import { NavLink } from "react-router-dom";
import { Pagination } from "antd";
const { Meta } = Card;

export default function CarouselItemSearch({ dataElearning }) {
  const renderKhoaHoc1 = (item, index) => {
    return (
      <Card
        key={index}
        hoverable
        style={{
          width: "100%",
        }}
        cover={
          <img alt="example" src={item.hinhAnh} className="object-cover h-52" />
        }
      >
        <Meta title={item.tenKhoaHoc} />
        <div className="flex justify-end mt-4">
          <NavLink
            to={`/detail/${item.maKhoaHoc}`}
            className="px-4 py-2 text-white bg-yellow-500 rounded shadow  hover:text-white"
          >
            Chi Tiết
          </NavLink>
        </div>
      </Card>
    );
  };
  const [start, setStart] = useState(0);
  const [end, setEnd] = useState(8);
  const pageSize = 8;
  return (
    <div>
      <div className="grid grid-cols-4 gap-6">
        {dataElearning &&
          dataElearning
            .slice(start, end)
            .map((item) => <div>{renderKhoaHoc1(item)}</div>)}
      </div>
      <div className="flex items-center justify-center my-4">
        <Pagination
          defaultCurrent={1}
          defaultPageSize={1}
          total={dataElearning.length}
          onChange={(numPage) => {
            setStart((numPage - 1) * pageSize);
            setEnd((numPage - 1) * pageSize + pageSize);
          }}
          pageSize={pageSize}
        />
      </div>
    </div>
  );
}
