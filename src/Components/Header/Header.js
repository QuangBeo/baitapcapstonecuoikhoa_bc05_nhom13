import React from "react";
import UserNav from "./UserNav";
import logo from "./logo/logo.png";
import Category from "../Category/Category";
import SearchKhoaHoc from "../Search/Search";

export default function Header() {
  return (
    <div className="flex justify-between items-center bg-white shadow-md shadow-gray-300 px-10 py-2">
      <a href="/">
        <img className="h-[60px] w-auto" src={logo} alt="" />
      </a>
      <Category />
      <SearchKhoaHoc />
      <UserNav />
    </div>
  );
}
