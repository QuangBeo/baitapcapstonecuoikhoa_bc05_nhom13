import React, { useState } from "react";
import { Card } from "antd";
import { NavLink } from "react-router-dom";
import { Pagination } from "antd";
const { Meta } = Card;

export default function CarouselItemDanhMuc({ dataDanhMuc }) {
  const renderKhoaHoc1 = (item, index) => {
    return (
      <Card
        key={index}
        hoverable
        style={{
          width: "100%",
        }}
        cover={
          <img alt="example" src={item.hinhAnh} className="h-52 object-cover" />
        }
      >
        <Meta title={item.tenKhoaHoc} />
        <div className="flex justify-end mt-4">
          <NavLink
            to={`/detail/${item.maKhoaHoc}`}
            className=" rounded bg-yellow-500 text-white py-2 px-4 shadow hover:text-white"
          >
            Chi Tiết
          </NavLink>
        </div>
      </Card>
    );
  };

  const [start, setStart] = useState(0);
  const [end, setEnd] = useState(12);
  const pageSize = 12;

  return (
    <div>
      <div className="grid grid-cols-4 gap-6">
        {dataDanhMuc &&
          dataDanhMuc
            .slice(start, end)
            .map((item) => <div>{renderKhoaHoc1(item)}</div>)}
      </div>
      <div className="flex justify-center items-center my-4">
        <Pagination
          defaultCurrent={1}
          defaultPageSize={1}
          total={dataDanhMuc.length || 0}
          onChange={(numPage) => {
            setStart((numPage - 1) * pageSize);
            setEnd((numPage - 1) * pageSize + pageSize);
          }}
          pageSize={pageSize}
        />
      </div>
    </div>
  );
}
