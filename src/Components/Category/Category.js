import React, { useEffect, useState } from "react";
import { FaList } from "react-icons/fa";
import { Dropdown, Space } from "antd";
import { elearningService } from "../../Services/elearningService";
import { NavLink } from "react-router-dom";

export default function Category() {
  const [dataCategory, setdataCategory] = useState([]);

  useEffect(() => {
    elearningService
      .getDanhMucKhoaHoc()
      .then((res) => {
        setdataCategory(res.data);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  const renderDanhMucKhoaHoc = () => {
    return dataCategory.map((item) => {
      return {
        key: item.maDanhMuc,
        label: (
          <NavLink to={`/category/${item.maDanhMuc}`}>
            {item.tenDanhMuc}
          </NavLink>
        ),
      };
    });
  };
  const items = renderDanhMucKhoaHoc();
  return (
    <div>
      <Dropdown
        className="hover:cursor-pointer hover:text-red-500 px-4 py-2 border-1 border-ray"
        trigger={["click"]}
        menu={{ items }}
      >
        <a onClick={(e) => e.preventDefault()}>
          <Space className=" text-xl ">
            <FaList />
            Danh Mục Khoá Học
          </Space>
        </a>
      </Dropdown>
    </div>
  );
}
