import React from "react";
import { Input, message, Space } from "antd";
import { useNavigate } from "react-router-dom";
const { Search } = Input;

export default function SearchKhoaHoc() {
  const navigate = useNavigate();
  const onSearch = (value) => {
    if (value) {
      navigate(`/search/${value}`);
    } else {
      message.error("VUI LÒNG NHẬP TÊN KHOÁ HỌC");
      navigate("/");
    }
  };
  return (
    <div>
      <Space direction="vertical">
        <Search
          placeholder="Nhập tên khoá học"
          onSearch={onSearch}
          size="large"
          allowClear
        />
      </Space>
    </div>
  );
}
