import React from "react";
import { FaStar } from "react-icons/fa";

export default function CarouselDetail({ dataDetail }) {
  const tenDanhMuc = dataDetail?.danhMucKhoaHoc?.tenDanhMucKhoaHoc;
  return (
    <div
      className="bg-cover"
      style={{
        width: "100%",
        height: "500px",
        backgroundPosition: "50% 50%",
        backgroundImage:
          "url(https://as1.ftcdn.net/v2/jpg/03/52/39/00/1000_F_352390061_Bem8aYkzfGhIObTC4fXhf0PmKQjWM1wN.jpg)",
      }}
    >
      <div className="flex justify-end ml-14 items-center h-full">
        <div className="w-1/3 text-gray-200 font-bold">
          <h1 className="text-5xl w-1/2 uppercase">{tenDanhMuc}</h1>
          <p className="text-gray-200 text-xl font-light py-10">
            Đánh giá khoá học <FaStar className="inline ml-4 text-green-700" />
            <FaStar className="inline  ml-2 text-green-700" />
            <FaStar className="inline  ml-2 text-green-700" />
            <FaStar className="inline  ml-2 text-green-700" />
            <FaStar className="inline  ml-2 text-green-700" />
          </p>
          <div>
            <button className="px-8 py-2  text-gray-200 border-2 border-gray-200 rounded-3xl hover:text-gray-200 hover:bg-yellow-600 hover:border-yellow-600">
              Đăng ký
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
