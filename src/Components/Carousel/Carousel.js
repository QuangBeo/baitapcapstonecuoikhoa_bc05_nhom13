import React from "react";

export default function Carousel() {
  return (
    <div
      className="bg-cover"
      style={{
        width: "100%",
        height: "500px",
        backgroundPosition: "50% 50%",
        backgroundImage:
          "url(https://thumbs.dreamstime.com/b/web-coding-software-development-concept-banner-laptop-program-code-screen-download-cloud-service-remote-work-245636154.jpg)",
      }}
    >
      <div className="flex items-center justify-start w-full h-full pl-14">
        <div className="w-1/3 font-bold text-yellow-600">
          <h1 className="w-1/2 text-5xl">KHỞI ĐẦU SỰ NGHIỆP CỦA BẠN</h1>
          <h3 className="py-6 text-2xl text-gray-200">
            Trở thành lập trình viên chuyên nghiệp tại CyberSoft
          </h3>
          <div>
            <button className="px-4 py-2 text-gray-200 bg-yellow-600 border-2 border-yellow-600 ">
              Xem Khoá Học
            </button>
            <button className="px-4 py-2 ml-6 text-gray-200 border-2 border-gray-200 hover:text-gray-200 hover:bg-yellow-600 hover:border-yellow-600">
              Tư Vấn Học
            </button>
          </div>
        </div>
      </div>
    </div>
  );
}
